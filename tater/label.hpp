#pragma once

namespace tater
{
  /// Represents the address of an instruction in an instruction stream.
  ///
  /// \todo Really, we want this to represent the address of a basic
  /// block, but since we don't have blocks in the program yet, it doesn't
  /// really matter.
  ///
  /// \todo Make *this* a type of value?
  using Label = unsigned;

} // namespace tater
