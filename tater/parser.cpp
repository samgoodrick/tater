#include <tater/parser.hpp>
#include <tater/function.hpp>
#include <tater/instruction.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>

namespace tater
{
  static std::string
  read_file(std::istream& is)
  {
    using Iter = std::istreambuf_iterator<char>;
    return std::string(Iter(is), Iter());
  }

  static std::string
  read_file(std::string const& path)
  {
    std::ifstream ifs(path);
    return read_file(ifs);
  }

  Parser::Parser(std::string const& path)
    : m_path(path), m_text(read_file(path))
  {
    init();
  }

  Parser::Parser(std::istream& is)
    : m_path(), m_text(read_file(is))
  {
    init();
  }

  void
  Parser::init()
  {
    // Stack operations.
    m_parsers["push"] = &Parser::parse_push_instruction;
    m_parsers["pop"] = &Parser::parse_pop_instruction;
    m_parsers["copy"] = &Parser::parse_copy_instruction;
    m_parsers["swap"] = &Parser::parse_swap_instruction;
    m_parsers["fpush"] = &Parser::parse_fpush_instruction;
    m_parsers["fpop"] = &Parser::parse_fpop_instruction;
    m_parsers["fcopy"] = &Parser::parse_fcopy_instruction;
    m_parsers["fswap"] = &Parser::parse_fswap_instruction;
    
    // Integer operations.
    m_parsers["add"] = &Parser::parse_add_instruction;
    m_parsers["sub"] = &Parser::parse_sub_instruction;
    m_parsers["mul"] = &Parser::parse_mul_instruction;
    m_parsers["div"] = &Parser::parse_div_instruction;
    m_parsers["rem"] = &Parser::parse_rem_instruction;
    m_parsers["cmp"] = &Parser::parse_cmp_instruction;

    // Float operations.
    m_parsers["fadd"] = &Parser::parse_fadd_instruction;
    m_parsers["fsub"] = &Parser::parse_fsub_instruction;
    m_parsers["fmul"] = &Parser::parse_fmul_instruction;
    m_parsers["fdiv"] = &Parser::parse_fdiv_instruction;
    m_parsers["fcmp"] = &Parser::parse_fcmp_instruction;

    // Branching operations
    m_parsers["bif"] = &Parser::parse_bif_instruction;
    m_parsers["br"] = &Parser::parse_br_instruction;

    // Program execution
    m_parsers["halt"] = &Parser::parse_halt_instruction;

    // FXIME: Add more parsers.    

    // Initialize the type table
    m_types["int"] = int_type;
    m_types["float"] = float_type;
    m_types["addr"] = addr_type;
    m_types["fn"] = fn_type;
  }

  Function*
  Parser::parse_function()
  {
    // FIXME: Stop leaking memory.
    m_fn = new Function;

    std::istringstream ss(m_text);

    // populate the label table in a first pass so that
    // branching to later program areas is possible
    for (std::string line; std::getline(ss, line); ) {
      if (line.empty())
	continue;
      parse_label(line);
    }

    std::istringstream ss2(m_text);
    for (std::string line; std::getline(ss2, line); ) {
      if (line.empty())
        continue;
      parse_line(line);
    }

    return m_fn;
  }

  template<typename T, typename... Args>
  static void 
  make_insn(Function* fn, Args&&... args)
  {
    Instruction* insn = new T(std::forward<Args>(args)...);
    fn->add_instruction(insn);
  }

  void
  Parser::parse_label(std::string const& str)
  {
    std::size_t n = str.find(':');

    if(n != std::string::npos)
    {
      std::string label_name(str, 0, n);
      auto label_iter = m_labels.find(label_name);
      if(label_iter == m_labels.end())
	m_labels.insert({label_name, current_line});
    }

    ++current_line;
  }

  void
  Parser::parse_line(std::string const& str)
  {
    std::size_t n = str.find_first_of(':');

    // we have already parsed all the labels, just skip this line
    // if it is a label, but still add it to the function.
    if(n != std::string::npos) {
      make_insn<Label_instruction>(m_fn);
      return;
    }
    else
    {
      n = str.find_first_of(' ');
      std::string op(str, 0, n);
      std::string rest;
      if (n != std::string::npos)
	rest = std::string(str, n + 1, str.size() - n);

      auto iter = m_parsers.find(op);
      if (iter == m_parsers.end()) {
	std::stringstream ss;
	ss << "unknown opcode '" << op << "'";
	throw std::runtime_error(ss.str());
      }
      auto parse = iter->second;

      // Invoke the parser.
      (this->*parse)(rest);
    }   
  }

  /// push n
  void
  Parser::parse_push_instruction(std::string const& rest)
  {
    char* end;
    Int_value n = std::strtol(rest.c_str(), &end, 10);
    make_insn<Push_instruction>(m_fn, Value(n));
  }

  /// pop t
  void
  Parser::parse_pop_instruction(std::string const& rest)
  {
    auto iter = m_types.find(rest);
    if (iter == m_types.end())
      throw std::runtime_error("invalid type");
    make_insn<Pop_instruction>(m_fn, iter->second);
  }

  /// copy
  void
  Parser::parse_copy_instruction(std::string const& rest)
  {    
    make_insn<Copy_instruction>(m_fn);
  }

  /// swap
  void
  Parser::parse_swap_instruction(std::string const& rest)
  {
    make_insn<Swap_instruction>(m_fn);
  }

  // fpush
  void
  Parser::parse_fpush_instruction(std::string const& rest)
  {
    char* end;
    Float_value n = std::strtof(rest.c_str(), &end);
    make_insn<Fpush_instruction>(m_fn, Value(n));
  }

  // fpop
  void
  Parser::parse_fpop_instruction(std::string const& rest)
  {
    auto iter = m_types.find(rest);
    if (iter == m_types.end())
      throw std::runtime_error("invalid type");
    make_insn<Fpop_instruction>(m_fn, iter->second);
  }

  // fcopy
  void
  Parser::parse_fcopy_instruction(std::string const& rest)
  {
    make_insn<Fcopy_instruction>(m_fn);
  }

  // fswap
  void
  Parser::parse_fswap_instruction(std::string const& rest)
  {
    make_insn<Fswap_instruction>(m_fn);
  }

  /// add
  void
  Parser::parse_add_instruction(std::string const& rest)
  {
    make_insn<Add_instruction>(m_fn);
  }

  /// sub
  void
  Parser::parse_sub_instruction(std::string const& rest)
  {
    make_insn<Sub_instruction>(m_fn);
  }

  /// mul
  void
  Parser::parse_mul_instruction(std::string const& rest)
  {
    make_insn<Mul_instruction>(m_fn);
  }

  /// div
  void
  Parser::parse_div_instruction(std::string const& rest)
  {
    make_insn<Div_instruction>(m_fn);
  }

  /// rem
  void
  Parser::parse_rem_instruction(std::string const& rest)
  {
    make_insn<Rem_instruction>(m_fn);
  }

  /// cmp
  void
  Parser::parse_cmp_instruction(std::string const& rest)
  {
    auto iter = m_operators.find(rest);
    if (iter == m_operators.end())
    {
      std::stringstream ss;
      ss << "Invalid comparison operator: '" << rest << '\n';
      throw std::runtime_error(ss.str());
    }

    make_insn<Cmp_instruction>(m_fn, iter->second);
  }

  void
  Parser::parse_fadd_instruction(std::string const& rest)
  {
    make_insn<Fadd_instruction>(m_fn);
  }

  void
  Parser::parse_fsub_instruction(std::string const& rest)
  {
    make_insn<Fsub_instruction>(m_fn);
  }

  void
  Parser::parse_fmul_instruction(std::string const& rest)
  {
    make_insn<Fmul_instruction>(m_fn);
  }

  void
  Parser::parse_fdiv_instruction(std::string const& rest)
  {
    make_insn<Fdiv_instruction>(m_fn);
  }

  void
  Parser::parse_fcmp_instruction(std::string const& rest)
  {
    auto iter = m_operators.find(rest);
    if (iter == m_operators.end())
    {
      std::stringstream ss;
      ss << "Invalid comparison operator: '" << rest << '\n';
      throw std::runtime_error(ss.str());
    }

    make_insn<Fcmp_instruction>(m_fn, iter->second);
  }

  // bif
  void
  Parser::parse_bif_instruction(std::string const& rest)
  {
    std::size_t n = rest.find_first_of(' ');

    if(n != std::string::npos)
    {
      std::string t_label_name(rest, 0, n);
      std::string f_label_name(rest, n + 1, rest.size());

      auto iter = m_labels.find(t_label_name);
      if(iter == m_labels.end())
      {
	std::stringstream ss;
	ss << "Label '" << t_label_name << "' was not defined";
	throw std::runtime_error(ss.str());
      }
      Label t_label = iter->second;

      iter = m_labels.find(f_label_name);
      if(iter == m_labels.end())
      {
	std::stringstream ss;
	ss << "Label '" << f_label_name << "' was not defined";
	throw std::runtime_error(ss.str());
      }
      Label f_label = iter->second;

      make_insn<Bif_instruction>(m_fn, t_label, f_label);
    }
    else
    {
      std::stringstream ss;
      ss << "not enough arguments in bif instruction: 'bif " << rest << "'";
      throw std::runtime_error(ss.str());
    }
  }

  // br
  void
  Parser::parse_br_instruction(std::string const& rest)
  {
    if(rest.size())
    {
      std::string br_label_name = rest;

      auto iter = m_labels.find(br_label_name);
      if(iter == m_labels.end())
      {
	std::stringstream ss;
	ss << "Label '" << br_label_name << "' was not defined";
	throw std::runtime_error(ss.str());
      }
      Label br_label = iter->second;

      make_insn<Br_instruction>(m_fn, br_label);
    }
    else
    {
      std::stringstream ss;
      ss << "not enough arguments in br instruction: 'br " << rest << "'";
      throw std::runtime_error(ss.str());
    }
  }

  /// halt
  void
  Parser::parse_halt_instruction(std::string const& rest)
  {
    make_insn<Halt_instruction>(m_fn);
  }

} // namespace tater
