#pragma once

#include <tater/type.hpp>
#include <tater/label.hpp>
#include <tater/instruction.hpp>

#include <iosfwd>
#include <string>
#include <unordered_map>

namespace tater
{
  class Function;

  /// The parser is responsible for returning a program (or module?) from
  /// an input file.
  ///
  /// \todo Stop using strings to manage parsing and build a real parser.
  /// This is way to fragile in its current form.
  class Parser
  {
  public:
    Parser(std::string const& path);
    /// Construct a parser for the given file.

    Parser(std::istream& is);
    /// Construct a parser for the given input stream.

    Function* parse_function();
    /// Parse a function definition. For now, this is simply an unnamed
    /// list of instructions. Obviously, we'd like to  make this more
    /// meaningful.    

  private:
    void parse_line(std::string const& line);
    /// Parse a line of text.

    void parse_label(std::string const& line);

    // Instruction parsers
    
    void parse_push_instruction(std::string const& rest);
    /// Parses a push instruction.

    void parse_pop_instruction(std::string const& rest);
    /// Parses a pop instruction.

    void parse_copy_instruction(std::string const& rest);
    /// Parses a copy instruction.

    void parse_swap_instruction(std::string const& rest);
    /// Parses a swap instruction.

    void parse_add_instruction(std::string const& rest);
    /// Parses an add instruction.

    void parse_fpush_instruction(std::string const& rest);
    /// Parses an fpush instruction.

    void parse_fpop_instruction(std::string const& rest);
    /// Parses an fpop instruction.

    void parse_fcopy_instruction(std::string const& rest);
    /// Parses an fcopy instruction.

    void parse_fswap_instruction(std::string const& rest);
    /// Parses an fswap instruction.

    void parse_sub_instruction(std::string const& rest);
    /// Parses a sub instruction.

    void parse_mul_instruction(std::string const& rest);
    /// Parses a mul instruction.

    void parse_div_instruction(std::string const& rest);
    /// Parses a div instruction

    void parse_rem_instruction(std::string const& rest);
    /// Parses a rem instruction

    void parse_cmp_instruction(std::string const& rest);
    /// Parses a cmp instruction

    void parse_fadd_instruction(std::string const& rest);
    /// Parses an fadd instruction.

    void parse_fsub_instruction(std::string const& rest);
    /// Parses an fsub instruction.

    void parse_fmul_instruction(std::string const& rest);
    /// Parses an fmul instruction.

    void parse_fdiv_instruction(std::string const& rest);
    /// Parses an fdiv instruction

    void parse_fcmp_instruction(std::string const& rest);
    /// Parses a cmp instruction

    void parse_bif_instruction(std::string const& rest);
    /// Parses a bif instruction

    void parse_br_instruction(std::string const& rest);
    /// Parses a br instruction

    void parse_halt_instruction(std::string const& rest);
    // Parse a halt instruction.

    void init();
    /// Perform common initialization.

  private:
    std::string m_path;
    /// The path to the file.

    std::string m_text;
    /// The text of the file.

    Function* m_fn;
    /// The current function.

    using line = Label;

    line current_line = 0;

    using Parse_fn = void (Parser::*)(std::string const&);
    /// A function that parses the remainder of a string.

    std::unordered_map<std::string, Parse_fn> m_parsers;
    /// Associates each opcode and directive with a parser for its
    /// remaining text.

    std::unordered_map<std::string, Type> m_types;
    std::unordered_map<std::string, Label> m_labels;

    std::unordered_map<std::string, Instruction::Operator> m_operators =
    {
      {"eq", Instruction::Operator::eq},
      {"ne", Instruction::Operator::ne},
      {"slt", Instruction::Operator::slt},
      {"ult", Instruction::Operator::ult},
      {"flt", Instruction::Operator::flt},
      {"sgt", Instruction::Operator::sgt},
      {"ugt", Instruction::Operator::ugt},
      {"fgt", Instruction::Operator::fgt},
      {"sle", Instruction::Operator::sle},
      {"ule", Instruction::Operator::ule},
      {"fle", Instruction::Operator::fle},
      {"sge", Instruction::Operator::sge},
      {"uge", Instruction::Operator::uge},
      {"fge", Instruction::Operator::fge},
    };
  };

} // namespace tater
